// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Camera based hand tracking driver code.
 * @author Christoph Haag <christtoph.haag@collabora.com>
 * @author Moses Turner <mosesturner@protonmail.com>
 * @author Nova King <technobaboo@protonmail.com>
 * @ingroup drv_ulv4
 */

#include "LeapC.h"

#include "ulv4_driver.h"
#include "ulv4_util.h"
#include "../north_star/ns_expose_tracker.h"

#include "os/os_time.h"
#include "math.h"
#include "math/m_space.h"
#include "util/u_device.h"
#include "util/u_var.h"
#include "util/u_debug.h"
#include <string.h>
#include <time.h>

struct ulv4_device
{
	struct xrt_device base;

	LEAP_CONNECTION* leap_connection;

	struct xrt_tracking_origin tracking_origin;

	enum u_logging_level ll;
};

enum xrt_space_relation_flags valid_flags =
	(enum xrt_space_relation_flags)(XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |
									XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT |
									XRT_SPACE_RELATION_POSITION_VALID_BIT |
									XRT_SPACE_RELATION_POSITION_TRACKED_BIT);

struct xrt_quat leap_oxr_rot = {0};

DEBUG_GET_ONCE_LOG_OPTION(ulv4_log, "ULV4_LOG", U_LOGGING_WARN)

#define ULV4_TRACE(ulv4d, ...) U_LOG_XDEV_IFL_T(&ulv4d->base, ulv4d->ll, __VA_ARGS__)
#define ULV4_DEBUG(ulv4d, ...) U_LOG_XDEV_IFL_D(&ulv4d->base, ulv4d->ll, __VA_ARGS__)
#define ULV4_INFO(ulv4d, ...) U_LOG_XDEV_IFL_I(&ulv4d->base, ulv4d->ll, __VA_ARGS__)
#define ULV4_WARN(ulv4d, ...) U_LOG_XDEV_IFL_W(&ulv4d->base, ulv4d->ll, __VA_ARGS__)
#define ULV4_ERROR(ulv4d, ...) U_LOG_XDEV_IFL_E(&ulv4d->base, ulv4d->ll, __VA_ARGS__)

static inline struct ulv4_device *
ulv4_device(struct xrt_device *xdev)
{
	return (struct ulv4_device *)xdev;
}

static void
ulv4_device_update_inputs(struct xrt_device *xdev)
{
	// Empty
}

void ulv4_leap_convert_joint_xrt(LEAP_VECTOR position, LEAP_QUATERNION rotation, float width, uint32_t joint_index, struct xrt_hand_joint_set *out_set)
{
	struct xrt_hand_joint_value *joint = &out_set->values.hand_joint_set_default[joint_index];
	struct xrt_pose *pose = &joint->relation.pose;
	pose->position.x = -position.x / 1000.0f;
	pose->position.y = -position.z / 1000.0f;
	pose->position.z = -position.y / 1000.0f;
	pose->orientation.w =  rotation.w;
	pose->orientation.x = -rotation.x;
	pose->orientation.y = -rotation.z;
	pose->orientation.z = -rotation.y;

	math_quat_rotate(&pose->orientation, &leap_oxr_rot, &pose->orientation);

	joint->radius = width / 1000 / 2; // Leap is in mm, OpenXR in m

	joint->relation.relation_flags = valid_flags;
}

void ulv4_leap_convert_digit_xrt(LEAP_DIGIT digit, uint32_t metacarpal_joint, struct xrt_hand_joint_set *out_set)
{
	uint8_t digit_joint_start = 0;
	if(metacarpal_joint == XRT_HAND_JOINT_THUMB_METACARPAL)
		digit_joint_start = 1; // In Leap v4, the thumb is missing the metacarpal joint

	// Convert all bones except for the tip
	LEAP_BONE bone = digit.bones[0];
	for(uint8_t j=digit_joint_start; j<4; ++j) {
		bone = digit.bones[j];
		ulv4_leap_convert_joint_xrt(bone.prev_joint, bone.rotation, bone.width, metacarpal_joint+j-digit_joint_start, out_set);
	}

	// Then convert the tip
	ulv4_leap_convert_joint_xrt(bone.next_joint, bone.rotation, bone.width, metacarpal_joint+4-digit_joint_start, out_set);
}

void ulv4_leap_convert_hand_xrt(LEAP_HAND* hand, struct xrt_hand_joint_set *out_set)
{
	// Convert the wrist (wrist is treated as the arm bone closest to the hand aka farthest from heart)
	ulv4_leap_convert_joint_xrt(hand->arm.next_joint, hand->arm.rotation, hand->arm.width, XRT_HAND_JOINT_WRIST, out_set);

	// Convert the palm
	LEAP_BONE *middle_metacarpal = &hand->middle.metacarpal;
	LEAP_VECTOR palm_position = {0};

	palm_position.x = (middle_metacarpal->prev_joint.x + middle_metacarpal->next_joint.x) / 2;
	palm_position.y = (middle_metacarpal->prev_joint.y + middle_metacarpal->next_joint.y) / 2;
	palm_position.z = (middle_metacarpal->prev_joint.z + middle_metacarpal->next_joint.z) / 2;

	ulv4_leap_convert_joint_xrt(palm_position, middle_metacarpal->rotation, hand->palm.width, XRT_HAND_JOINT_PALM, out_set);

	// Convert all the digits
	ulv4_leap_convert_digit_xrt(hand->pinky,  XRT_HAND_JOINT_LITTLE_METACARPAL, out_set);
	ulv4_leap_convert_digit_xrt(hand->ring,   XRT_HAND_JOINT_RING_METACARPAL,   out_set);
	ulv4_leap_convert_digit_xrt(hand->middle, XRT_HAND_JOINT_MIDDLE_METACARPAL, out_set);
	ulv4_leap_convert_digit_xrt(hand->index,  XRT_HAND_JOINT_INDEX_METACARPAL,  out_set);
	ulv4_leap_convert_digit_xrt(hand->thumb,  XRT_HAND_JOINT_THUMB_METACARPAL,  out_set);
}

static void
ulv4_device_get_hand_tracking(struct xrt_device *xdev,
							enum xrt_input_name name,
							uint64_t at_timestamp_ns,
							struct xrt_hand_joint_set *out_value)
{
	struct ulv4_device *ulv4d = ulv4_device(xdev);

	if (name == XRT_INPUT_GENERIC_HAND_TRACKING_LEFT)
		ULV4_TRACE(ulv4d, "Get left hand tracking data");
	else if (name == XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT)
		ULV4_TRACE(ulv4d, "Get right hand tracking data");
	else {
		ULV4_ERROR(ulv4d, "unknown input name for hand tracker");
		return;
	}

	LEAP_TRACKING_EVENT *frame = GetFrame();
	if(frame){
		// Process the hands
		for(uint32_t h = 0; h < frame->nHands; h++) {
			LEAP_HAND* hand = &frame->pHands[h];

			// Only one being true is possible
			bool left  = hand->type == eLeapHandType_Left  && name == XRT_INPUT_GENERIC_HAND_TRACKING_LEFT;
			bool right = hand->type == eLeapHandType_Right && name == XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT;

			if(!(left || right)) // The hand we're trying to get is not visible
				continue;

			out_value->isActive = true;
			ulv4_leap_convert_hand_xrt(hand, out_value);

			struct xrt_space_graph xsg = {0};

			struct xrt_space_relation ns_relation = {0};
			if (tracker_device != NULL) { // WILL be null if the realsense is not plugged in
				tracker_device->get_tracked_pose(tracker_device, XRT_INPUT_GENERIC_HEAD_POSE, at_timestamp_ns, &ns_relation);
				m_space_graph_add_relation(&xsg, nose_bridge_to_leap_motion_controller);
				m_space_graph_add_relation(&xsg, &ns_relation);
			}

			m_space_graph_resolve(&xsg, &out_value->hand_pose);
			out_value->hand_pose.relation_flags = valid_flags;

			return;
		}

		out_value->isActive = false;
		ULV4_TRACE(ulv4d, "Hand not visible");
		return;
	}

	ULV4_ERROR(ulv4d, "Leap interpolation has failed");
}

static void
ulv4_device_destroy(struct xrt_device *xdev)
{
	struct ulv4_device *ulv4d = ulv4_device(xdev);

	// Remove the variable tracking.
	u_var_remove_root(ulv4d);

	u_device_free(&ulv4d->base);
}

void
ulv4_create_leap_rot() {
	struct xrt_vec3 forward_axis = {0, 0, 1};
	struct xrt_vec3 right_axis = {1, 0, 0};

	struct xrt_quat rot1 = {0};
	math_quat_from_angle_vector(M_PI, &forward_axis, &rot1);

	struct xrt_quat rot2 = {0};
	math_quat_from_angle_vector(-M_PI/2, &right_axis, &rot2);

	math_quat_rotate(&rot1, &rot2, &leap_oxr_rot);
}

struct xrt_device *
ulv4_device_create(struct xrt_auto_prober *xap,
				 cJSON *attached_data,
				 struct xrt_prober *xp)
{
	enum u_device_alloc_flags flags = U_DEVICE_ALLOC_NO_FLAGS;

	//! @todo 2 hands hardcoded
	int num_hands = 2;

	struct ulv4_device *ulv4d =
		U_DEVICE_ALLOCATE(struct ulv4_device, flags, num_hands, 0);

	ulv4d->base.tracking_origin = &ulv4d->tracking_origin;
	ulv4d->base.tracking_origin->type = XRT_TRACKING_TYPE_EXTERNAL;
	ulv4d->base.tracking_origin->offset.position.x = 0.0f;
	ulv4d->base.tracking_origin->offset.position.y = 0.0f;
	ulv4d->base.tracking_origin->offset.position.z = 0.0f;
	ulv4d->base.tracking_origin->offset.orientation.w = 1.0f;

	ulv4_create_leap_rot();

	ulv4d->ll = debug_get_log_option_ulv4_log();

	ulv4d->base.update_inputs = ulv4_device_update_inputs;
	ulv4d->base.get_hand_tracking = ulv4_device_get_hand_tracking;
	ulv4d->base.destroy = ulv4_device_destroy;

	strncpy(ulv4d->base.str, "Camera based Hand Tracker",
			XRT_DEVICE_NAME_LEN);

	ulv4d->base.inputs[0].name = XRT_INPUT_GENERIC_HAND_TRACKING_LEFT;
	ulv4d->base.inputs[1].name = XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT;

	ulv4d->base.name = XRT_DEVICE_HAND_TRACKER;

	u_var_add_root(ulv4d, "Camera based Hand Tracker", true);
	u_var_add_ro_text(ulv4d, ulv4d->base.str, "Name");

	ulv4d->leap_connection = OpenConnection();

	LEAP_DEVICE_INFO* deviceProps = GetDeviceProperties();
	if(deviceProps) {
		switch(deviceProps->pid) {
			case eLeapDevicePID_Unknown: {
				ULV4_DEBUG(ulv4d, "Some unknown Ultraleap device initialized!");
			} break;

			case eLeapDevicePID_Peripheral: {
				ULV4_DEBUG(ulv4d, "Leap Motion Controller initialized!");
			} break;

			case eLeapDevicePID_Dragonfly: {
				ULV4_DEBUG(ulv4d, "You managed to get your hands on an Ulraleap Dragonfly and are using it with Monado :0");
			} break;

			case eLeapDevicePID_Nightcrawler: {
				ULV4_DEBUG(ulv4d, "How did you get an Ulraleap Nightcrawler? Anyway, it's initialized now.");
			} break;

			case eLeapDevicePID_Rigel: {
				ULV4_DEBUG(ulv4d, "Ulraleap Rigel initialized!");
			} break;

			default: {
				ULV4_DEBUG(ulv4d, "Some invalid Ulraleap device is initialized!");
			} break;
		}
	}

	return &ulv4d->base;
}
