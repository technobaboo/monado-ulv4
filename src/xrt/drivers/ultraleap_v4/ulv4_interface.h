// Copyright 2029, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Interface to camera based hand tracking driver code.
 * @author Christoph Haag <christoph.haag@collabora.com>
 * @author Moses Turner <mosesturner@protonmail.com>
 * @author Nova King <technobaboo@protonmail.com>
 * @ingroup drv_ulv4
 */

#pragma once

#include "math/m_api.h"
#include "xrt/xrt_device.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @defgroup drv_ulv4 Camera based hand tracking
 * @ingroup drv
 *
 * @brief
 */

/*!
 * Create a probe for camera based hand tracking.
 *
 * @ingroup drv_ulv4
 */
struct xrt_auto_prober *
ulv4_create_auto_prober();

/*!
 * @dir drivers/handtracking
 *
 * @brief @ref drv_ulv4 files.
 */
#ifdef __cplusplus
}
#endif
