// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Interface to camera based hand tracking driver code.
 * @author Christoph Haag <christtoph.haag@collabora.com>
 * @author Moses Turner <mosesturner@protonmail.com>
 * @author Nova King <technobaboo@protonmail.com>
 * @ingroup drv_ulv4
 */

#pragma once

#include "math/m_api.h"
#include "xrt/xrt_device.h"
#include "xrt/xrt_prober.h"

#ifdef __cplusplus
extern "C" {
#endif

struct xrt_device *
ulv4_device_create();


#ifdef __cplusplus
}
#endif
