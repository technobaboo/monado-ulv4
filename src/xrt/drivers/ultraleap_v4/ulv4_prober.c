// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Camera based hand tracking prober code.
 * @author Christoph Haag <christoph.haag@collabora.com>
 * @author Moses Turner <mosesturner@protonmail.com>
 * @author Nova King <technobaboo@protonmail.com>
 * @ingroup drv_ulv4
 */


#include "xrt/xrt_prober.h"

#include "util/u_misc.h"

#include "ulv4_interface.h"
#include "ulv4_driver.h"

/*!
 * @implements xrt_auto_prober
 */
struct ulv4_prober
{
	struct xrt_auto_prober base;
};

//! @private @memberof ulv4_prober
static inline struct ulv4_prober *
ulv4_prober(struct xrt_auto_prober *p)
{
	return (struct ulv4_prober *)p;
}

//! @public @memberof ulv4_prober
static void
ulv4_prober_destroy(struct xrt_auto_prober *p)
{
	struct ulv4_prober *ulv4p = ulv4_prober(p);

	free(ulv4p);
}

//! @public @memberof ulv4_prober
static struct xrt_device *
ulv4_prober_autoprobe(struct xrt_auto_prober *xap,
                    cJSON *attached_data,
                    bool no_hmds,
                    struct xrt_prober *xp)
{
	struct xrt_device *xdev = ulv4_device_create(xap, attached_data, xp);

	if (xdev == NULL) {
		return NULL;
	}

	xdev->orientation_tracking_supported = true;
	xdev->position_tracking_supported = true;
	xdev->hand_tracking_supported = true;
	xdev->device_type = XRT_DEVICE_TYPE_HAND_TRACKER;

	return xdev;
}

struct xrt_auto_prober *
ulv4_create_auto_prober()
{
	struct ulv4_prober *ulv4p = U_TYPED_CALLOC(struct ulv4_prober);
	ulv4p->base.name = "Camera Hand Tracking";
	ulv4p->base.destroy = ulv4_prober_destroy;
	ulv4p->base.lelo_dallas_autoprobe = ulv4_prober_autoprobe;

	return &ulv4p->base;
}
