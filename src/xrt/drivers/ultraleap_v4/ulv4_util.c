#include "ulv4_util.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#if defined(_WIN32)
	#include <Windows.h>
	#include <process.h>
	#define LockMutex EnterCriticalSection
	#define UnlockMutex LeaveCriticalSection
#else
	// #include <unistd.h>
	#include <pthread.h>
	#define LockMutex pthread_mutex_lock
	#define UnlockMutex pthread_mutex_unlock
#endif

#if defined(_MSC_VER)
	static void serviceMessageLoop(void * unused);
#else
	static void* serviceMessageLoop(void * unused);
#endif
static void setFrame(const LEAP_TRACKING_EVENT *frame);
static void setDevice(const LEAP_DEVICE_INFO *deviceProps);

bool IsConnected = false;

static volatile bool _isRunning = false;
static LEAP_CONNECTION connectionHandle = NULL;
static LEAP_TRACKING_EVENT *lastFrame = NULL;
static LEAP_DEVICE_INFO *lastDevice = NULL;

struct Callbacks ConnectionCallbacks;

#if defined(_MSC_VER)
static HANDLE pollingThread;
static CRITICAL_SECTION dataLock;
#else
static pthread_t pollingThread;
static pthread_mutex_t dataLock;
#endif

LEAP_CONNECTION* OpenConnection(){
	if(_isRunning){
		return &connectionHandle;
	}
	if(connectionHandle || LeapCreateConnection(NULL, &connectionHandle) == eLeapRS_Success){
		eLeapRS result = LeapOpenConnection(connectionHandle);
		if(result == eLeapRS_Success){
			_isRunning = true;
#if defined(_MSC_VER)
			InitializeCriticalSection(&dataLock);
			pollingThread = (HANDLE)_beginthread(serviceMessageLoop, 0, NULL);
#else
			pthread_create(&pollingThread, NULL, serviceMessageLoop, NULL);
#endif
		}
	}
	return &connectionHandle;
}

void CloseConnection(){
	if(!_isRunning){
		return;
	}
	_isRunning = false;
	LeapCloseConnection(connectionHandle);
#if defined(_MSC_VER)
	WaitForSingleObject(pollingThread, INFINITE);
	CloseHandle(pollingThread);
#else
	pthread_join(pollingThread, NULL);
#endif
}

void DestroyConnection(){
	CloseConnection();
	LeapDestroyConnection(connectionHandle);
}


void CloseConnectionHandle(LEAP_CONNECTION* connectionHandle){
	LeapDestroyConnection(*connectionHandle);
	_isRunning = false;
}

static void handleConnectionEvent(const LEAP_CONNECTION_EVENT *connection_event){
	IsConnected = true;
	if(ConnectionCallbacks.on_connection){
		ConnectionCallbacks.on_connection();
	}
}

static void handleConnectionLostEvent(const LEAP_CONNECTION_LOST_EVENT *connection_lost_event){
	IsConnected = false;
	if(ConnectionCallbacks.on_connection_lost){
		ConnectionCallbacks.on_connection_lost();
	}
}

static void handleDeviceEvent(const LEAP_DEVICE_EVENT *device_event){
	LEAP_DEVICE deviceHandle;

	eLeapRS result = LeapOpenDevice(device_event->device, &deviceHandle);
	if(result != eLeapRS_Success){
		printf("Could not open device %s.\n", ResultString(result));
		return;
	}


	LEAP_DEVICE_INFO deviceProperties;
	deviceProperties.size = sizeof(deviceProperties);


	deviceProperties.serial_length = 1;
	deviceProperties.serial = malloc(deviceProperties.serial_length);


	result = LeapGetDeviceInfo(deviceHandle, &deviceProperties);
	if(result == eLeapRS_InsufficientBuffer){

		deviceProperties.serial = realloc(deviceProperties.serial, deviceProperties.serial_length);
		result = LeapGetDeviceInfo(deviceHandle, &deviceProperties);
		if(result != eLeapRS_Success){
			printf("Failed to get device info %s.\n", ResultString(result));
			free(deviceProperties.serial);
			return;
		}
	}
	setDevice(&deviceProperties);
	if(ConnectionCallbacks.on_device_found){
		ConnectionCallbacks.on_device_found(&deviceProperties);
	}

	free(deviceProperties.serial);
	LeapCloseDevice(deviceHandle);
}

static void handleDeviceLostEvent(const LEAP_DEVICE_EVENT *device_event){
	if(ConnectionCallbacks.on_device_lost){
		ConnectionCallbacks.on_device_lost();
	}
}

static void handleDeviceFailureEvent(const LEAP_DEVICE_FAILURE_EVENT *device_failure_event){
	if(ConnectionCallbacks.on_device_failure){
		ConnectionCallbacks.on_device_failure(device_failure_event->status, device_failure_event->hDevice);
	}
}

static void handleTrackingEvent(const LEAP_TRACKING_EVENT *tracking_event){
	setFrame(tracking_event);
	if(ConnectionCallbacks.on_frame){
		ConnectionCallbacks.on_frame(tracking_event);
	}
}

static void handleLogEvent(const LEAP_LOG_EVENT *log_event){
	if(ConnectionCallbacks.on_log_message){
		ConnectionCallbacks.on_log_message(log_event->severity, log_event->timestamp, log_event->message);
	}
}

static void handleLogEvents(const LEAP_LOG_EVENTS *log_events){
	if(ConnectionCallbacks.on_log_message){
		for (int i = 0; i < (int)(log_events->nEvents); i++) {
			const LEAP_LOG_EVENT* log_event = &log_events->events[i];
			ConnectionCallbacks.on_log_message(log_event->severity, log_event->timestamp, log_event->message);
		}
	}
}

static void handlePolicyEvent(const LEAP_POLICY_EVENT *policy_event){
	if(ConnectionCallbacks.on_policy){
		ConnectionCallbacks.on_policy(policy_event->current_policy);
	}
}

static void handleConfigChangeEvent(const LEAP_CONFIG_CHANGE_EVENT *config_change_event){
	if(ConnectionCallbacks.on_config_change){
		ConnectionCallbacks.on_config_change(config_change_event->requestID, config_change_event->status);
	}
}

static void handleConfigResponseEvent(const LEAP_CONFIG_RESPONSE_EVENT *config_response_event){
	if(ConnectionCallbacks.on_config_response){
		ConnectionCallbacks.on_config_response(config_response_event->requestID, config_response_event->value);
	}
}

static void handleImageEvent(const LEAP_IMAGE_EVENT *image_event) {
	if(ConnectionCallbacks.on_image){
		ConnectionCallbacks.on_image(image_event);
	}
}

static void handlePointMappingChangeEvent(const LEAP_POINT_MAPPING_CHANGE_EVENT *point_mapping_change_event) {
	if(ConnectionCallbacks.on_point_mapping_change){
		ConnectionCallbacks.on_point_mapping_change(point_mapping_change_event);
	}
}

static void handleHeadPoseEvent(const LEAP_HEAD_POSE_EVENT *head_pose_event) {
		if(ConnectionCallbacks.on_head_pose){
			ConnectionCallbacks.on_head_pose(head_pose_event);
		}
}

#if defined(_MSC_VER)
static void serviceMessageLoop(void * unused){
#else
static void* serviceMessageLoop(void * unused){
#endif
	eLeapRS result;
	LEAP_CONNECTION_MESSAGE msg;
	while(_isRunning){
		unsigned int timeout = 1000;
		result = LeapPollConnection(connectionHandle, timeout, &msg);

		if(result != eLeapRS_Success){
			printf("LeapC PollConnection call was %s.\n", ResultString(result));
			continue;
		}

		switch (msg.type){
			case eLeapEventType_Connection:
				handleConnectionEvent(msg.connection_event);
				break;
			case eLeapEventType_ConnectionLost:
				handleConnectionLostEvent(msg.connection_lost_event);
				break;
			case eLeapEventType_Device:
				handleDeviceEvent(msg.device_event);
				break;
			case eLeapEventType_DeviceLost:
				handleDeviceLostEvent(msg.device_event);
				break;
			case eLeapEventType_DeviceFailure:
				handleDeviceFailureEvent(msg.device_failure_event);
				break;
			case eLeapEventType_Tracking:
				handleTrackingEvent(msg.tracking_event);
				break;
			case eLeapEventType_ImageComplete:

				break;
			case eLeapEventType_ImageRequestError:

				break;
			case eLeapEventType_LogEvent:
				handleLogEvent(msg.log_event);
				break;
			case eLeapEventType_Policy:
				handlePolicyEvent(msg.policy_event);
				break;
			case eLeapEventType_ConfigChange:
				handleConfigChangeEvent(msg.config_change_event);
				break;
			case eLeapEventType_ConfigResponse:
				handleConfigResponseEvent(msg.config_response_event);
				break;
			case eLeapEventType_Image:
				handleImageEvent(msg.image_event);
				break;
			case eLeapEventType_PointMappingChange:
				handlePointMappingChangeEvent(msg.point_mapping_change_event);
				break;
			case eLeapEventType_LogEvents:
				handleLogEvents(msg.log_events);
				break;
			case eLeapEventType_HeadPose:
				handleHeadPoseEvent(msg.head_pose_event);
				break;
			// default:

				// printf("Unhandled message type %i.\n", msg.type);
		}
	}
#if !defined(_MSC_VER)
	return NULL;
#endif
}

void setFrame(const LEAP_TRACKING_EVENT *frame){
	LockMutex(&dataLock);
	if(!lastFrame) lastFrame = malloc(sizeof(*frame));
	*lastFrame = *frame;
	UnlockMutex(&dataLock);
}

LEAP_TRACKING_EVENT* GetFrame(){
	LEAP_TRACKING_EVENT *currentFrame;

	LockMutex(&dataLock);
	currentFrame = lastFrame;
	UnlockMutex(&dataLock);

	return currentFrame;
}

static void setDevice(const LEAP_DEVICE_INFO *deviceProps){
	LockMutex(&dataLock);
	if(lastDevice){
		free(lastDevice->serial);
	} else {
		lastDevice = malloc(sizeof(*deviceProps));
	}
	*lastDevice = *deviceProps;
	lastDevice->serial = malloc(deviceProps->serial_length);
	memcpy(lastDevice->serial, deviceProps->serial, deviceProps->serial_length);
	UnlockMutex(&dataLock);
}

LEAP_DEVICE_INFO* GetDeviceProperties(){
	LEAP_DEVICE_INFO *currentDevice;
	LockMutex(&dataLock);
	currentDevice = lastDevice;
	UnlockMutex(&dataLock);
	return currentDevice;
}

const char* ResultString(eLeapRS r) {
	switch(r){
		case eLeapRS_Success:									return "eLeapRS_Success";
		case eLeapRS_UnknownError:						 return "eLeapRS_UnknownError";
		case eLeapRS_InvalidArgument:					return "eLeapRS_InvalidArgument";
		case eLeapRS_InsufficientResources:		return "eLeapRS_InsufficientResources";
		case eLeapRS_InsufficientBuffer:			 return "eLeapRS_InsufficientBuffer";
		case eLeapRS_Timeout:									return "eLeapRS_Timeout";
		case eLeapRS_NotConnected:						 return "eLeapRS_NotConnected";
		case eLeapRS_HandshakeIncomplete:			return "eLeapRS_HandshakeIncomplete";
		case eLeapRS_BufferSizeOverflow:			 return "eLeapRS_BufferSizeOverflow";
		case eLeapRS_ProtocolError:						return "eLeapRS_ProtocolError";
		case eLeapRS_InvalidClientID:					return "eLeapRS_InvalidClientID";
		case eLeapRS_UnexpectedClosed:				 return "eLeapRS_UnexpectedClosed";
		case eLeapRS_UnknownImageFrameRequest: return "eLeapRS_UnknownImageFrameRequest";
		case eLeapRS_UnknownTrackingFrameID:	 return "eLeapRS_UnknownTrackingFrameID";
		case eLeapRS_RoutineIsNotSeer:				 return "eLeapRS_RoutineIsNotSeer";
		case eLeapRS_TimestampTooEarly:				return "eLeapRS_TimestampTooEarly";
		case eLeapRS_ConcurrentPoll:					 return "eLeapRS_ConcurrentPoll";
		case eLeapRS_NotAvailable:						 return "eLeapRS_NotAvailable";
		case eLeapRS_NotStreaming:						 return "eLeapRS_NotStreaming";
		case eLeapRS_CannotOpenDevice:				 return "eLeapRS_CannotOpenDevice";
		default:															 return "unknown result type.";
	}
}
void millisleep(int milliseconds) {
#ifdef _WIN32
	Sleep(milliseconds);
#else
	struct timespec ts;
	ts.tv_sec = milliseconds / 1e9;             // whole seconds
	ts.tv_nsec = (milliseconds % (10^9)) * 1e6;    // remainder, in nanoseconds
	nanosleep(&ts, NULL);
#endif
}
