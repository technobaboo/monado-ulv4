#pragma once

extern struct xrt_device *tracker_device;

extern struct xrt_space_relation *nose_bridge_to_leap_motion_controller;
extern float *hand_min_confidence;
