// Copyright 2020, Collabora, Ltd., Moses Turner
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief   Driver for Ultraleap's V2 API for the Leap Motion Controller.
 * @author  Moses Turner <mosesturner@protonmail.com>
 * @author  Christoph Haag <christoph.haag@collabora.com>
 * @ingroup drv_ulv2
 */

#pragma once

#include "math/m_api.h"
#include "xrt/xrt_device.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @defgroup drv_ulv2 Camera based hand tracking
 * @ingroup drv
 *
 * @brief
 */

/*!
 * Create a probe for camera based hand tracking.
 *
 * @ingroup drv_ulv2
 */
struct xrt_auto_prober *
ulv2_create_auto_prober();

/*!
 * @dir drivers/handtracking
 *
 * @brief @ref drv_ulv2 files.
 */
#ifdef __cplusplus
}
#endif
