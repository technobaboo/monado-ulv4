// Copyright 2020, Collabora, Ltd., Moses Turner
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief   Driver for Ultraleap's V2 API for the Leap Motion Controller.
 * @author  Moses Turner <mosesturner@protonmail.com>
 * @author  Christoph Haag <christoph.haag@collabora.com>
 * @ingroup drv_ulv2
 */

#pragma once

#include "math/m_api.h"
#include "xrt/xrt_device.h"
#include "xrt/xrt_prober.h"

#ifdef __cplusplus
extern "C" {
#endif



struct xrt_device *
ulv2_device_create(struct xrt_auto_prober *xap,
                   cJSON *attached_data,
                   struct xrt_prober *xp);



#ifdef __cplusplus
}
#endif
