// Copyright 2020, Collabora, Ltd., Moses Turner
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief   Driver for Ultraleap's V2 API for the Leap Motion Controller.
 * @author  Moses Turner <mosesturner@protonmail.com>
 * @author  Christoph Haag <christoph.haag@collabora.com>
 * @ingroup drv_ulv2
 */

#include "ulv2_driver.h"
#include "util/u_device.h"
#include "util/u_var.h"
#include "util/u_debug.h"
#include "math/m_space.h"
#include "math/m_api.h"
#include "util/u_time.h"
#include "os/os_time.h"

#include "../north_star/ns_expose_tracker.h"

#include "Leap.h"

#include <string.h>
#include <pthread.h>

extern "C" {
struct ulv2_device
{
	struct xrt_device base;

	struct xrt_tracked_hand *tracker;

	struct xrt_space_relation hand_relation[2];
	struct u_hand_tracking u_tracking[2];

	struct xrt_tracking_origin tracking_origin;

	enum u_logging_level ll;
};

static inline struct ulv2_device *
ulv2_device(struct xrt_device *xdev)
{
	return (struct ulv2_device *)xdev;
}
}

DEBUG_GET_ONCE_LOG_OPTION(ulv2_log, "ulv2_LOG", U_LOGGING_WARN)

#define ULV2_TRACE(ulv2d, ...)                                                 \
	U_LOG_XDEV_IFL_T(&ulv2d->base, ulv2d->ll, __VA_ARGS__)
#define ULV2_DEBUG(ulv2d, ...)                                                 \
	U_LOG_XDEV_IFL_D(&ulv2d->base, ulv2d->ll, __VA_ARGS__)
#define ULV2_INFO(ulv2d, ...)                                                  \
	U_LOG_XDEV_IFL_I(&ulv2d->base, ulv2d->ll, __VA_ARGS__)
#define ULV2_WARN(ulv2d, ...)                                                  \
	U_LOG_XDEV_IFL_W(&ulv2d->base, ulv2d->ll, __VA_ARGS__)
#define ULV2_ERROR(ulv2d, ...)                                                 \
	U_LOG_XDEV_IFL_E(&ulv2d->base, ulv2d->ll, __VA_ARGS__)

#define printf_pose(pose)                                                      \
	printf("%f %f %f  %f %f %f %f\n", pose.position.x, pose.position.y,    \
	       pose.position.z, pose.orientation.x, pose.orientation.y,        \
	       pose.orientation.z, pose.orientation.w);


// Leap Motion integration start

pthread_t input_pthread;
pthread_attr_t input_pthread_attrs;
bool pthread_should_continue = true;

xrt_hand_joint_set my_left_hand;
xrt_hand_joint_set my_right_hand;

pthread_mutex_t lock_yours;

xrt_hand_joint_set your_left_hand;
xrt_hand_joint_set your_right_hand;
bool your_left_hand_exists;
bool your_right_hand_exists;

enum xrt_space_relation_flags valid_flags =
    (enum xrt_space_relation_flags)(XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |
                                    XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT |
                                    XRT_SPACE_RELATION_POSITION_VALID_BIT |
                                    XRT_SPACE_RELATION_POSITION_TRACKED_BIT);

enum hand_joint_matrix_type
{
	right_finger_thumb,
	left_finger_thumb,
	right_palm_wrist,
	left_palm_wrist,
};

static void
zldtt(struct xrt_hand_joint_value *joint,
      Leap::Vector jointposition,
      Leap::Matrix jointbasis,
      float width,
      hand_joint_matrix_type hmtype)
{
	struct xrt_space_relation *relation = &joint->relation;
	joint->radius = (width / 1000) / 2;
	xrt_vec3 multiplymatrix = {1, 1, 1};


	float xmulprime = -1 / 1000.0;
	float ymulprime = -1 / 1000.0;
	float zmulprime = -1 / 1000.0;

	struct xrt_matrix_3x3 turn_into_quat;
#define mat jointbasis

	switch (hmtype) {
	case right_finger_thumb:
		multiplymatrix = {-1, -1, -1};
		turn_into_quat = {-mat.xBasis.x, -mat.yBasis.x, -mat.zBasis.x,
		                  -mat.xBasis.z, -mat.yBasis.z, -mat.zBasis.z,
		                  -mat.xBasis.y, -mat.yBasis.y, -mat.zBasis.y};
		break;
	case left_finger_thumb:
		turn_into_quat = {mat.xBasis.x, -mat.yBasis.x, -mat.zBasis.x,
		                  mat.xBasis.z, -mat.yBasis.z, -mat.zBasis.z,
		                  mat.xBasis.y, -mat.yBasis.y, -mat.zBasis.y};
		break;
	case right_palm_wrist: {
		struct xrt_matrix_3x3 intermediate = {
		    -mat.xBasis.x, -mat.yBasis.x, -mat.zBasis.x,
		    -mat.xBasis.z, -mat.yBasis.z, -mat.zBasis.z,
		    -mat.xBasis.y, -mat.yBasis.y, -mat.zBasis.y};
		const struct xrt_matrix_3x3 mul = {1, 0, 0, 0, 0, -1, 0, 1, 0};
		math_matrix_3x3_multiply(&intermediate, &mul, &turn_into_quat);
	} break;
	case left_palm_wrist: {
		struct xrt_matrix_3x3 intermediate = {
		    mat.xBasis.x, -mat.yBasis.x, -mat.zBasis.x,
		    mat.xBasis.z, -mat.yBasis.z, -mat.zBasis.z,
		    mat.xBasis.y, -mat.yBasis.y, -mat.zBasis.y};
		const struct xrt_matrix_3x3 mul = {1, 0, 0, 0, 0, -1, 0, 1, 0};
		math_matrix_3x3_multiply(&intermediate, &mul, &turn_into_quat);
	} break;
	}

	math_quat_from_matrix_3x3(&turn_into_quat, &relation->pose.orientation);
	relation->pose.position.x = jointposition.x * xmulprime;
	relation->pose.position.y = jointposition.z * ymulprime;
	relation->pose.position.z = jointposition.y * zmulprime;
	relation->relation_flags = valid_flags;
}

void *
heres_the_pthread(void *ptr_to_xdev)
{
	Leap::Controller LeapController;
	struct xrt_device *xdev = (struct xrt_device *)ptr_to_xdev;
	struct ulv2_device *ulv2d = ulv2_device(xdev);
	bool hmdpolicyset = true;
	while (pthread_should_continue) {
		if (LeapController.isConnected()) {
			if (!hmdpolicyset) {
				ULV2_INFO(ulv2d, "policy not set yet\n");
				LeapController.setPolicy(
				    Leap::Controller::POLICY_OPTIMIZE_HMD);
				hmdpolicyset = LeapController.isPolicySet(
				    Leap::Controller::POLICY_OPTIMIZE_HMD);
				if (hmdpolicyset) {
					ULV2_INFO(ulv2d, "policy set\n");
				}
			}
			Leap::Frame frame = LeapController.frame();
			Leap::HandList hands = frame.hands();
			bool leftbeendone = false;
			bool rightbeendone = false;
			for (Leap::HandList::const_iterator hl = hands.begin();
			     hl != hands.end(); ++hl) {
				bool cl;
				const Leap::Hand hand = *hl;
				xrt_hand_joint_set *put_in_hand;
				if (hand.confidence() < *hand_min_confidence)
					continue;
				if (hand.isLeft()) {
					if (leftbeendone)
						continue;
					put_in_hand = &my_left_hand;
					leftbeendone = true;
					cl = true;
				}
				if (!hand.isLeft()) {
					if (rightbeendone)
						continue;
					// Leap API is totally cool with there
					// being three hands.
					// If it thinks there are two left
					// hands, for example, in the XR client
					// the hand will flicker between the two
					// hands it detects.
					put_in_hand = &my_right_hand;
					rightbeendone = true;
					cl = false;
				}

#define e(y) &put_in_hand->values.hand_joint_set_default[y]

				zldtt(e(XRT_HAND_JOINT_PALM),
				      hand.palmPosition(), hand.basis(), 50,
				      cl ? left_palm_wrist : right_palm_wrist);
				zldtt(e(XRT_HAND_JOINT_WRIST),
				      hand.wristPosition(), hand.arm().basis(),
				      50,
				      cl ? left_palm_wrist : right_palm_wrist);

				const Leap::FingerList fingers = hand.fingers();

				// Bunch of macros to make the following
				// boilerplate easier to deal with

#define fb(y) finger.bone(y)
#define prevJ(y) finger.bone(y).prevJoint()
#define nextJ(y) finger.bone(y).nextJoint()

#define lm Leap::Bone::TYPE_METACARPAL
#define lp Leap::Bone::TYPE_PROXIMAL
#define li Leap::Bone::TYPE_INTERMEDIATE
#define ld Leap::Bone::TYPE_DISTAL

				for (Leap::FingerList::const_iterator fl =
				         fingers.begin();
				     fl != fingers.end(); ++fl) {
					const Leap::Finger finger = *fl;
					Leap::Finger::Type fingerType =
					    finger.type();
					hand_joint_matrix_type er =
					    cl ? left_finger_thumb
					       : right_finger_thumb;
					switch (fingerType) {
					// clang-format off
						case Leap::Finger::Type::TYPE_THUMB:
							zldtt(e(XRT_HAND_JOINT_THUMB_METACARPAL),	   prevJ(lp),fb(lp).basis(),fb(lp).width(), er);
							zldtt(e(XRT_HAND_JOINT_THUMB_PROXIMAL),      prevJ(li),fb(li).basis(),fb(lp).width(), er);
							zldtt(e(XRT_HAND_JOINT_THUMB_DISTAL),		     prevJ(ld),fb(ld).basis(),fb(li).width(), er);
							zldtt(e(XRT_HAND_JOINT_THUMB_TIP),				   nextJ(ld),fb(ld).basis(),fb(ld).width(), er);
							// this looks wrong, right? Check https://developer-archive.leapmotion.com/documentation/v2/cpp/devguide/Leap_Overview.html before yelling at me
							// remember, thumbs have four joints/three bones, while the rest of your fingers have five joints/four bones
							break;
						case Leap::Finger::Type::TYPE_INDEX:
							zldtt(e(XRT_HAND_JOINT_INDEX_METACARPAL),    prevJ(lm),fb(lm).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_INDEX_PROXIMAL), 		 prevJ(lp),fb(lp).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_INDEX_INTERMEDIATE),	 prevJ(li),fb(li).basis(),fb(lp).width(), er);
							zldtt(e(XRT_HAND_JOINT_INDEX_DISTAL), 			 prevJ(ld),fb(ld).basis(),fb(li).width(), er);
							zldtt(e(XRT_HAND_JOINT_INDEX_TIP), 					 nextJ(ld),fb(ld).basis(),fb(ld).width(), er);
							break;
						case Leap::Finger::Type::TYPE_MIDDLE:
							zldtt(e(XRT_HAND_JOINT_MIDDLE_METACARPAL), 	 prevJ(lm),fb(lm).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_MIDDLE_PROXIMAL), 		 prevJ(lp),fb(lp).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_MIDDLE_INTERMEDIATE), prevJ(li),fb(li).basis(),fb(lp).width(), er);
							zldtt(e(XRT_HAND_JOINT_MIDDLE_DISTAL), 			 prevJ(ld),fb(ld).basis(),fb(li).width(), er);
							zldtt(e(XRT_HAND_JOINT_MIDDLE_TIP),          nextJ(ld),fb(ld).basis(),fb(ld).width(), er);
							break;
						case Leap::Finger::Type::TYPE_RING:
							zldtt(e(XRT_HAND_JOINT_RING_METACARPAL),     prevJ(lm),fb(lm).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_RING_PROXIMAL),  	 	 prevJ(lp),fb(lp).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_RING_INTERMEDIATE), 	 prevJ(li),fb(li).basis(),fb(lp).width(), er);
							zldtt(e(XRT_HAND_JOINT_RING_DISTAL), 				 prevJ(ld),fb(ld).basis(),fb(li).width(), er);
							zldtt(e(XRT_HAND_JOINT_RING_TIP), 					 nextJ(ld),fb(ld).basis(),fb(ld).width(), er);
							break;
						case Leap::Finger::Type::TYPE_PINKY:
							zldtt(e(XRT_HAND_JOINT_LITTLE_METACARPAL), 	 prevJ(lm),fb(lm).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_LITTLE_PROXIMAL), 		 prevJ(lp),fb(lp).basis(),fb(lm).width(), er);
							zldtt(e(XRT_HAND_JOINT_LITTLE_INTERMEDIATE), prevJ(li),fb(li).basis(),fb(lp).width(), er);
							zldtt(e(XRT_HAND_JOINT_LITTLE_DISTAL), 			 prevJ(ld),fb(ld).basis(),fb(li).width(), er);
							zldtt(e(XRT_HAND_JOINT_LITTLE_TIP),					 nextJ(ld),fb(ld).basis(),fb(ld).width(), er);
							break;
						// clang-format on
					}
				}
			}
			pthread_mutex_lock(&lock_yours);
			memcpy(&your_right_hand, &my_right_hand,
			       sizeof(struct xrt_hand_joint_set));
			memcpy(&your_left_hand, &my_left_hand,
			       sizeof(struct xrt_hand_joint_set));
			your_left_hand_exists = leftbeendone;
			your_right_hand_exists = rightbeendone;
			pthread_mutex_unlock(&lock_yours);
		} else {
			ULV2_ERROR(ulv2d, "LeapController is not connected\n");
		}
	}
	// code only reaches here after pthread_should_continue turns false in
	// ulv2_device_destroy
	pthread_exit(NULL);
}

// Leap Motion integration end

extern "C" {

static void
ulv2_device_update_inputs(struct xrt_device *xdev)
{
	// Empty
}

static void
ulv2_device_get_hand_tracking(struct xrt_device *xdev,
                              enum xrt_input_name name,
                              uint64_t at_timestamp_ns,
                              struct xrt_hand_joint_set *out_value)
{

	//! @todo this function doesn't do anything with at_timestamp_ns,
	// would be nice to add pose-prediction. Probably once LM v5 comes out

	struct ulv2_device *ulv2d = ulv2_device(xdev);

	bool hand_valid;

	switch (name) {
	case XRT_INPUT_GENERIC_HAND_TRACKING_LEFT:
		pthread_mutex_lock(&lock_yours);
		memcpy(out_value, &your_left_hand,
		       sizeof(struct xrt_hand_joint_set));
		hand_valid = your_left_hand_exists;
		pthread_mutex_unlock(&lock_yours);
		break;
	case XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT:
		pthread_mutex_lock(&lock_yours);
		memcpy(out_value, &your_right_hand,
		       sizeof(struct xrt_hand_joint_set));
		hand_valid = your_right_hand_exists;
		pthread_mutex_unlock(&lock_yours);
		break;
	default:
		ULV2_ERROR(ulv2d, "unknown input name for hand tracker");
		return;
	}

	if (hand_valid) {
		out_value->isActive = true;
		struct xrt_space_graph xsg = {};

		struct xrt_space_relation ns_relation;
		if (tracker_device !=
		    NULL) { // WILL be null if the realsense is not plugged in
			tracker_device->get_tracked_pose(
			    tracker_device, XRT_INPUT_GENERIC_HEAD_POSE,
			    at_timestamp_ns, &ns_relation);
			m_space_graph_add_relation(
			    &xsg, nose_bridge_to_leap_motion_controller);

			m_space_graph_add_relation(&xsg, &ns_relation);
		}


		m_space_graph_resolve(&xsg, &out_value->hand_pose);
		out_value->hand_pose.relation_flags = valid_flags;
	} else {
		out_value->isActive = false;
	}
}
}

static void
ulv2_device_destroy(struct xrt_device *xdev)
{
	struct ulv2_device *ulv2d = ulv2_device(xdev);

	pthread_should_continue = false;
	pthread_join(input_pthread, NULL);

	// Remove the variable tracking.
	u_var_remove_root(ulv2d);

	u_device_free(&ulv2d->base);
}

struct xrt_device *
ulv2_device_create(struct xrt_auto_prober *xap,
                   cJSON *attached_data,
                   struct xrt_prober *xp)
{
	enum u_device_alloc_flags flags = U_DEVICE_ALLOC_NO_FLAGS;

	int num_hands = 2;

	struct ulv2_device *ulv2d =
	    U_DEVICE_ALLOCATE(struct ulv2_device, flags, num_hands, 0);
	float retry_sleep_time = 0.25;
	float timeout = 2;
	int num_tries = (timeout / retry_sleep_time);
	bool succeeded_connected = false;
	bool succeeded_service_connected = false;

	Leap::Controller LeapController;
	os_nanosleep(1000000000 *
	             0.02); // sleep for a bit so that the LeapController can
	                    // connect to the service
	for (int i = 0; i < num_tries; i++) {
		succeeded_connected = LeapController.isConnected();
		succeeded_service_connected =
		    LeapController.isServiceConnected();
		if (!succeeded_connected) {
			if (succeeded_service_connected) {
				ULV2_INFO(ulv2d,
				          "Connected to Leap service, but not "
				          "connected to Leap Motion "
				          "controller. Retrying (%i / %i)",
				          i, num_tries);
			} else {
				ULV2_INFO(ulv2d,
				          "Not connected to Leap service. "
				          "Retrying (%i / %i)",
				          i, num_tries);
			}
		} else {
			ULV2_INFO(ulv2d, "Leap Motion controller connected!");
			break;
		}
		os_nanosleep(1000000000 *
		             retry_sleep_time); // 1 second * retry_sleep_time
	}
	if (!succeeded_connected) {
		if (succeeded_service_connected) {
			ULV2_INFO(ulv2d,
			          "Connected to Leap service, but couldn't "
			          "connect to leap motion controller.\n"
			          "Is it plugged in and has your Leap service "
			          "detected it?");
		} else {
			ULV2_INFO(ulv2d,
			          "Couldn't connect to Leap service. Try "
			          "running sudo leapd in another terminal.");
		}
		goto cleanup;
	}

	ulv2d->base.tracking_origin = &ulv2d->tracking_origin;
	ulv2d->base.tracking_origin->type = XRT_TRACKING_TYPE_EXTERNAL;
	ulv2d->base.tracking_origin->offset.position.x = 0.0f;
	ulv2d->base.tracking_origin->offset.position.y = 0.0f;
	ulv2d->base.tracking_origin->offset.position.z = 0.0f;
	ulv2d->base.tracking_origin->offset.orientation.w = 1.0f;

	ulv2d->ll = debug_get_log_option_ulv2_log();

	ulv2d->base.update_inputs = ulv2_device_update_inputs;
	ulv2d->base.get_hand_tracking = ulv2_device_get_hand_tracking;
	ulv2d->base.destroy = ulv2_device_destroy;

	strncpy(ulv2d->base.str, "Leap Motion v2 driver", XRT_DEVICE_NAME_LEN);

	ulv2d->base.inputs[0].name = XRT_INPUT_GENERIC_HAND_TRACKING_LEFT;
	ulv2d->base.inputs[1].name = XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT;

	ulv2d->base.name = XRT_DEVICE_HAND_TRACKER;

	u_var_add_root(ulv2d, "Leap Motion v2 driver", true);
	u_var_add_ro_text(ulv2d, ulv2d->base.str, "Name");
	pthread_mutex_init(&lock_yours, NULL);
	pthread_attr_init(&input_pthread_attrs);
	pthread_create(&input_pthread, &input_pthread_attrs,
	               (&heres_the_pthread), (void *)&ulv2d->base);

	ULV2_DEBUG(ulv2d, "Hand Tracker initialized!");

	return &ulv2d->base;

cleanup:
	ulv2_device_destroy(&ulv2d->base);
	return NULL;
}
