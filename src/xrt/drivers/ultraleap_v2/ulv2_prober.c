// Copyright 2020, Collabora, Ltd., Moses Turner
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief   Driver for Ultraleap's V2 API for the Leap Motion Controller.
 * @author  Moses Turner <mosesturner@protonmail.com>
 * @author  Christoph Haag <christoph.haag@collabora.com>
 * @ingroup drv_ulv2
 */


#include "xrt/xrt_prober.h"

#include "util/u_misc.h"
#include "util/u_debug.h"

#include "ulv2_interface.h"
#include "ulv2_driver.h"
#include <stdio.h>

DEBUG_GET_ONCE_OPTION(ulv2_config_path, "ULV2_CONFIG_PATH", NULL)

/*!
 * @implements xrt_auto_prober
 */
struct ulv2_prober
{
	struct xrt_auto_prober base;
};

//! @private @memberof ulv2_prober
static inline struct ulv2_prober *
ulv2_prober(struct xrt_auto_prober *p)
{
	printf("ulv2_prob called\n");

	return (struct ulv2_prober *)p;
}

//! @public @memberof ulv2_prober
static void
ulv2_prober_destroy(struct xrt_auto_prober *p)
{
	struct ulv2_prober *ulv2p = ulv2_prober(p);

	free(ulv2p);
}

//! @public @memberof ulv2_prober
static struct xrt_device *
ulv2_prober_autoprobe(struct xrt_auto_prober *xap,
                      cJSON *attached_data,
                      bool no_hmds,
                      struct xrt_prober *xp)
{
	printf("prober_autoprobe called\n");

	struct xrt_device *xdev = ulv2_device_create(xap, attached_data, xp);

	xdev->orientation_tracking_supported = true;
	xdev->position_tracking_supported = true;
	xdev->hand_tracking_supported = true;
	xdev->device_type = XRT_DEVICE_TYPE_HAND_TRACKER;

	return xdev;
}

struct xrt_auto_prober *
ulv2_create_auto_prober()
{
	printf("autoprober called\n");
	struct ulv2_prober *ulv2p = U_TYPED_CALLOC(struct ulv2_prober);
	ulv2p->base.name = "Camera Hand Tracking";
	ulv2p->base.destroy = ulv2_prober_destroy;
	ulv2p->base.lelo_dallas_autoprobe = ulv2_prober_autoprobe;

	return &ulv2p->base;
}
